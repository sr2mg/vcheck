'use strict';

//Lib
const express =require("express");
const app = express();
const FeedParser = require("feedparser");
const request = require("request");
const Youtube = require("youtube-node");
const https = require("https");
const fs = require('fs');

const Vtuberjson = require("./VTlist.json");

main();

async function main(){
    const getjson =await pushArr();
    const fileName="./public/VTuberfeed.json";
    const jsondata =JSON.stringify(getjson);
    fs.writeFile(fileName,jsondata,(err)=>{
        if(err) throw err;
        console.log("書き込みました。");
    });

}

async function pushArr(){
    let VJson=[];
    const APIKey =process.env.GATSBY_API_KEY;
    //緊急事態用。現在は使っていない。
    const APIKeyList=[
        process.env.GATSBY_API_0,
        process.env.GATSBY_API_1,
        process.env.GATSBY_API_2,
        process.env.GATSBY_API_3,
        process.env.GATSBY_API_4,
        process.env.GATSBY_API_5,
    ];
    for(let i=0;i<Vtuberjson.VT.length;i++){

        VJson.push(await ConnectAPI(`${Vtuberjson.VT[i].ID}`,APIKey));
    }
    return VJson;
}

async function ConnectAPI(ChID,APIKey){
    return new Promise((resolve,reject)=>{
        let youtube = new Youtube();

        const getURL="https://www.googleapis.com/youtube/v3/search?part=snippet&channelId="+ChID+"&key="+APIKey+"&order=date";
        const req = https.get(getURL,(res)=>{
            let body='';
    
            res.setEncoding('utf8');
            res.on('data',(chunk)=>{//ここ直す
                body+=chunk;
            });
    
            res.on('end',(res)=>{
                res = JSON.parse(body);
                let JudgeUpComing=0;
                let Vobject={};
                //jsonを文字列で出力
                for(let i=0;JudgeUpComing===0;i++){
                    if(res.items[i].snippet.liveBroadcastContent!=="upcoming"){
                        JudgeUpComing=1;
                        console.log(res.items[i].snippet.title+"を読み込みました。");
                        Vobject.title=res.items[i].snippet.title;
                        Vobject.VideoID=res.items[i].id.videoId;
                        Vobject.Published=res.items[i].snippet.publishedAt;
                        Vobject.author=res.items[i].snippet.channelTitle;
                        Vobject.ChID=ChID;
                    }
                }
    
                resolve(Vobject);
            });
        });
        req.end();
    })
}